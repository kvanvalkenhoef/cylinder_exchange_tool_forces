function varargout = GUI_v2(varargin)
% GUI MATLAB code for GUI_v2.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI

% Last Modified by GUIDE v2.5 27-Feb-2015 17:55:16

% Begin initialization code
clc
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @GUI_OpeningFcn, ...
    'gui_OutputFcn',  @GUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before GUI is made visible.
function GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI (see VARARGIN)

% Choose default command line output for GUI
handles.output = hObject;

% load images in GUI
% axes(handles.axes1);
% image(imread('arrowright2.png', 'png'))
% axis off
% axis equal

guidata(hObject, handles);
addpath(genpath('C:\Users\koen.vanvalkenhoef\Documents\GitRepos\ASL'))



% --- Executes during object creation, after setting all properties.
function text17_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% str = textread('changelog.txt');
fileID = fopen('changelog.txt');
text = textscan(fileID,'%s',2,'Delimiter', '\n');
app = text{1}(1);
lib = text{1}(2);
fclose(fileID);
set(hObject, 'String', [app; lib]);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% Type stut %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function editStutType_Callback(hObject, eventdata, handles)
% hObject    handle to edFx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
stuttype = get(hObject,'String');  %returns entry of edLoadCaseName
if str2num(stuttype) == 1
    stut_type_string = 'Rocket';
elseif str2num(stuttype) == 2
    stut_type_string = 'Cylinder';
else
    errordlg('You must enter a number: 1 or 2','Wrong Input','Modal');
    uicontrol(hObject);
    return;
end
setappdata(0,'stut_type',stut_type_string);

% save the changes to the structure
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function editStutType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edFx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% Push out length %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function editExtensionLength_Callback(hObject, eventdata, handles)
% hObject    handle to edFx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
extensionLength = get(hObject,'String');  %returns entry of edLoadCaseName
setappdata(0,'extensionLength',str2num(extensionLength));
% save the changes to the structure
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function editExtensionLength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edFx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% Cylinder Selector %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function editCylinder_Callback(hObject, eventdata, handles)
% hObject    handle to edFx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cylinderselection = get(hObject,'String');  %returns entry of edLoadCaseName
setappdata(0,'cylinderselection',ceil(str2num(cylinderselection)));
%     if rem(cylinderselection,1) == 0 % check if the remainder is zero
%         errordlg('The number must be an integer, please correct','Missing Input','Modal');
%     end
% save the changes to the structure
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function editCylinder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edFx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%%%%%%%%%%%%%%%%%%% POP MENU Load Cases %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on selection change in popupmenuLC.
function popupmenuLC_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuLC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuLC contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuLC

% contents = cellstr(get(hObject,'String')); % returns selected workspace
% choice = contents{get(hObject,'Value')};
switch get(hObject,'value')
    case 2
        LCWorkspace = 1;
    case 3
        LCWorkspace = 2;
    case 4
        LCWorkspace = 3;
end
setappdata(0,'LCWorkspace',LCWorkspace);

% set OM to SC if SC is selected
if LCWorkspace == 3;
    set(handles.popupmenuOM,'Value',4);
    setappdata(0,'LCOM',3);
    set(handles.popupmenuOM,'Enable','off');
else
    set(handles.popupmenuOM,'Enable','on');
end

% save the changes to the structure
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function popupmenuLC_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuLC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject,'Value',1)

% --- Executes on selection change in popupmenuOM.
function popupmenuOM_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuOM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuOM contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuOM
switch get(hObject,'value')
    case 2
        LCOM = 1;
    case 3
        LCOM = 2;
    case 4
        LCOM = 3;
end
setappdata(0,'LCOM',LCOM);
% save the changes to the structure
guidata(hObject,handles);

%%%%%%%%%%%%%%%%%%%% POP MENU Operating Modes %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes during object creation, after setting all properties.
function popupmenuOM_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuOM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject,'Value',1);

%%%%%%%%%%%%%%%%%%%% POP MENU Slideway Load Cases %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%% Slideway arm selection
% --- Executes on selection change in popupmenuswarm.
function popupmenuSWArm_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuswarm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuswarm contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuswarm
switch get(hObject,'value')
    case 2
        SWArm = 1;
    case 3
        SWArm = 2;
    case 4
        SWArm = 3;
end
setappdata(0,'SWArm',SWArm);

% save the changes to the structure
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function popupmenuSWArm_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuswarm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbRun.
function pbRun_Callback(hObject, eventdata, handles)
% hObject    handle to pbRun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc
%% Check all the entried variables

% check if basic geometric parameters are defined
if isempty(getappdata(0,'hexapod'))
    errordlg('You must define the basic geometric parameters','Missing Input','Modal');
    uicontrol(hObject);
    return;
end

% check if stut_type is defined
if isempty(getappdata(0,'stut_type'))
    errordlg('You must define a stut type','Missing Input','Modal');
    uicontrol(hObject);
    return;
end

% check if extension length is defined
if isempty(getappdata(0,'extensionLength'))
    errordlg('You must define an extension length','Missing Input','Modal');
    uicontrol(hObject);
    return;
end

% check if there is at least one loadcase defined
% if isempty(getappdata(0,'InputMatrix'))
%     errordlg('You must define at least one load case','Missing Input','Modal');
%     uicontrol(hObject);
%     return;
% end

% Configuration of geometrics
filename = getappdata(0,'filename');
hexapod = getappdata(0,'hexapod');
extensionLength = getappdata(0,'extensionLength');
cylinderselection = getappdata(0,'cylinderselection');
stut_type = getappdata(0,'stut_type');

% check if extension length is defined
if extensionLength  > hexapod.geometry.stroke
    errordlg('The extension length exceeds the stroke length, impossible hexapod attitude','Incorrect Input','Modal');
    uicontrol(hObject);
    return;
end

if  cylinderselection > 6 | cylinderselection < 1
    errordlg('There are only 6 cylinders, please enter a number between 1 and 6','Missing Input','Modal');
    uicontrol(hObject);
    return;
end

%% Properties for saving the file
fileName_Output_temp = char(inputdlg('Please enter the name for your file','Filename'));
if isempty(fileName_Output_temp)
    set(hObject,'Visible', 'on');
    return
end

directoryName = uigetdir('','Please select a folder to save to');
if directoryName == 0      % User pressed the "Cancel" button
    fileName_Output = inputdlg('Please enter the name for your file, + extension .xlsx','Filename', 1,{fileName_Output_temp});
    if isempty(fileName_Output_temp)
        set(hObject,'Visible', 'on');
        return
    end
end

% Loop over various load cases that are being changed in the selected load
% case file with a known layout.
sheet = 4; % set the sheet number - should be the overview IO sheet
GW_range = {'GXL', 'GXL Cargo'};
SF_range = {'Normal','Div SF v2','GXL Cargo SF'}; % Div SF v2, Div SF v1, Normal
% GW_range = {'GXL Cargo'};
% SF_range = {'GXL Cargo SF'}; % Div SF v2, Div SF v1, Normal
Slideway_range = {'N'}; % Y or N
Slideway_leg_range = [1,2,3]; % 1 2 of 3
acceleration_range = {'calm','normal'};
no_of_it = length(GW_range)*length(acceleration_range)*length(SF_range)*length(Slideway_range)*length(Slideway_leg_range)-(length(GW_range)*length(acceleration_range)*length(SF_range)*length(Slideway_range)*(length(Slideway_leg_range)-2));
counter_overall = 1; % define counter to check overal progress of job
counter_excel   = 0;
% Initializing overal progress waitbar
overallProgress = waitbar(0,'Overall progress...','Name','Please wait...');
keyboard
% start looping over all possible load cases
for gw = 1:length(GW_range)
    if counter_excel < counter_overall
        Excel = actxserver('Excel.Application'); % define excel active server
        invoke(Excel.Workbooks,'Open',filename); % open the speficied xls
        counter_excel = counter_excel + 1;
    end
    GW = GW_range(1); %
    xlRange_gw = 'R16';
    xlswritefast(filename,GW,sheet,xlRange_gw)
    for acc_cond = 1:length(acceleration_range)
        if counter_excel < counter_overall
            Excel = actxserver('Excel.Application'); % define excel active server
            invoke(Excel.Workbooks,'Open',filename); % open the speficied xls
            counter_excel = counter_excel + 1;
        end
        acceleration_cond = acceleration_range(acc_cond); % 1 2 of 3
        xlRange_acc_cond = 'P23';
        xlswritefast(filename,acceleration_cond,sheet,xlRange_acc_cond)
        % loop over the various types Starframe options
        for sf = 1:length(SF_range)
            if counter_excel < counter_overall
                Excel = actxserver('Excel.Application'); % define excel active server
                invoke(Excel.Workbooks,'Open',filename); % open the speficied xls
                counter_excel = counter_excel + 1;
            end
            SF = SF_range(sf);
            xlRange_sf = 'P12';
          
            xlswritefast(filename,SF,sheet,xlRange_sf)
            % loop over the with or without slideway possibility
            for sw = 1:length(Slideway_range)
                if counter_excel < counter_overall
                    Excel = actxserver('Excel.Application'); % define excel active server
                    invoke(Excel.Workbooks,'Open',filename); % open the speficied xls
                    counter_excel = counter_excel + 1;
                end
                Slideway = Slideway_range(sw);
                xlRange_sw = 'D25';
                % no Slideway if the GXL Cargo is analyzed.
                if strcmp('GXL Cargo',GW)
                    Slideway = 'N';
                end
                xlswritefast(filename,Slideway,sheet,xlRange_sw)
                
                % if no slideway is present there is no reason to loop over 3 legs, one is enough
                if strcmp('Y',Slideway)
                    no_of_legs = length(Slideway_leg_range);
                else
                    no_of_legs = 1;
                end
                % loop over the slideway leg possibilities
                for sw_leg = 1:no_of_legs
                    if counter_excel < counter_overall
                        Excel = actxserver('Excel.Application'); % define excel active server
                        invoke(Excel.Workbooks,'Open',filename); % open the speficied xls
                        counter_excel = counter_excel + 1;
                    end
                    Slideway_leg = Slideway_leg_range(sw_leg); % 1 2 of 3
                    xlRange_swleg = 'P20';
                    xlswritefast(filename,Slideway_leg,sheet,xlRange_swleg)
                    
                    % quit excel after writing values to the load case sheet
                    Excel.Quit
                    Excel.delete
                    clear Excel
                    
                    % adjust the fileoutput name per combination load case
                    fileName_Output = strcat(fileName_Output_temp,'_',GW,'_',SF,'_',Slideway,'_',num2str(Slideway_leg),'_',acceleration_cond);
                    
                    waitbar(counter_overall/no_of_it,overallProgress,sprintf('Overall progress... (%d/%d)',counter_overall,no_of_it));
                    
                    wait = waitbar(0, 'Loading load cases');
                    % readin the loadcase forces & moments
                    InputMatrix  = xlsread(filename, 'MATLAB');
                    
                    % Change all workspaces to CET workspace
                    InputMatrix(2,:) = 4;
                    waitbar(0.5,wait, 'Loading load cases')
                    [~, LCNameMatrix, ~] = xlsread(filename, 'MATLAB', 'B1:Z1');         

                    close(wait)
                    
                    % get loadcases
                    LCNameMatrix{length(LCNameMatrix)+1} = '-'; % add nameless cell in case there are no loadcases in either opererational mode
                    
                    % Initializing waitbar
                    waitBar = waitbar(0,'Getting parameters...','Name','Please wait...');
                    
                    % Adjust slew angles to cover full circle by default
                    slewMin  = 0;
                    slewStep = 5;
                    slewMax  = 360;
                    slewvec = slewMin:slewStep:slewMax;
                    
                    waitbar(0,waitBar,'...Determining workspace...')
                    
                    for cyl = 1:6%cylinderselection % loop over all cylinders
                        % Create the file name for the forces per cylinder
                        filePath_per_cylinder = char(fullfile(directoryName,strcat(fileName_Output ,'_cyl_' , num2str(cyl) ,  '.xlsx')));
                        
                        waitbar(0.5,waitBar,'...Determining workspace...')
                        % Attitude Hexapod from SC to extended
                        [workspaceOutputCET, cylinderLengthsCET, cylinderUnitVectorCET, ~,T_f] = computeAttitude(hexapod,cyl,extensionLength,1,stut_type);
                        waitbar(1,waitBar,'...Determining workspace...')
                        
                        % Initialize vector to store cylinder forces
                        FMatrix=zeros(size(workspaceOutputCET,1)*length(slewvec),6,size(InputMatrix,2)); % size of workspace*slewing angles,number of cylinders,number of loadcases,
                        % Compute cylinder forces
                        for lc = 1:size(InputMatrix,2) % loop for every column (load case)
                            
                            loadcase   = InputMatrix(4:9,lc); % dont take row nr 1 because this gives the workspace, nr 2 gives NO/EC info and nr 3 gives slideway arm connection
                            SWloadcase = InputMatrix(10:15,lc);% Slideway load cases
                            SWArm      = InputMatrix(3,lc);    % Starframe arm where slideway is mounted
                            
                            allForces = []; % initialize forces vector to save cylinder forces
                            for i = 1:length(slewvec)
                                [forces, cylinderUnitVectorCET] = computeCylinderExchangeToolForce( hexapod, 'unitvector', cylinderUnitVectorCET, loadcase, slewvec(i), SWArm, SWloadcase, T_f, workspaceOutputCET); % Compute the cylinder forces
                                allForces = [allForces; forces];
                                waitbar(lc/size(InputMatrix,2)/cyl,waitBar,sprintf('Calculating output forces... (%d/%d)',lc,size(InputMatrix,2)));
                            end
                            FMatrix(1:size(allForces,1),:,lc) = allForces; % store all cylinder forces in the initialized vector
                        end
                        
                        % duplicate lengths,unitvectors, basematrices, workspaces for every slew
                        % angle (needed for calc_lloydstable)
                        workspaceOutputCET    = repmat(workspaceOutputCET, length(slewvec),1);
                        cylinderUnitVectorCET = repmat(cylinderUnitVectorCET,[1 1 length(slewvec)]);
                        cylinderLengthsCET    = repmat(cylinderLengthsCET,length(slewvec),1);
                        % baseMatrixSC         = repmat(baseMatrixCET, [1 1 length(slewvec)]);
                        
                        
                        %% Calculate Lloyds table
                        waitbar(0,waitBar,sprintf('...Determining output tables... (%d/%d)',lc,size(InputMatrix,2)));
                        %calc_lloydstable; % output: Lloydsformtotal
                        
                        WSCET = [workspaceOutputCET zeros(size(workspaceOutputCET,1),1)];
                        
                        % add slew angles to workspace
                        slewAnglesCET = [];
                        
                        for slew = 1:length(slewvec);
                            vector = repmat(slewvec(slew),size(workspaceOutputCET,1)/length(slewvec),1);
                            slewAnglesCET = [slewAnglesCET; vector];
                        end
                        
                        WSCET(:,7) = slewAnglesCET;
                        
                        % put all cylinder displacements in same row
                        DirectVecSC = zeros(size(cylinderUnitVectorCET,3),18);
                        
                        % create directional vector elements for each cylinder
                        for i = 1:size(cylinderUnitVectorCET,3)
                            DirectVecSC(i,:) = [cylinderUnitVectorCET(1,:,i) cylinderUnitVectorCET(2,:,i) cylinderUnitVectorCET(3,:,i)];
                        end
                        
                        %% Write output to excel
                        Excel = actxserver('Excel.Application');
                        if ~exist(filePath_per_cylinder,'file')
                            ExcelWorkbook = Excel.workbooks.Add;
                            ExcelWorkbook.SaveAs(filePath_per_cylinder);
                        end
                        
                        invoke(Excel.Workbooks,'Open',filePath_per_cylinder);
                        
                        % define the output
                        for s = 1:size(InputMatrix,2)
                            steps = size(InputMatrix,2);
                            waitbar(1/17,waitBar,sprintf('...Writing to file... (%d/%d)',s,steps));
                            xlswritefast(filePath_per_cylinder,{'X [m]','Y [m] ','Z [m]','Phi [deg]','Theta [deg]','Psi [deg]', 'Slew [deg]'...
                                'x1', 'x2', 'x3', 'x4', 'x5', 'x6' 'y1', 'y2', 'y3', 'y4', 'y5', 'y6',...
                                'z1', 'z2', 'z3', 'z4', 'z5', 'z6'},LCNameMatrix{s},'A1');
                            xlswritefast(filePath_per_cylinder,{'L1 [m]','L2 [m]','L3 [m]','L4 [m]','L5 [m]','L6 [m]'},LCNameMatrix{s},'AP1');
                            waitbar(2/17,waitBar,sprintf('...Writing to file... (%d/%d)',s,steps));
                            xlswritefast(filePath_per_cylinder,WSCET,LCNameMatrix{s},'A2');
                            xlswritefast(filePath_per_cylinder,DirectVecSC,LCNameMatrix{s},'H2');
                            %xlswritefast(filePath_per_cylinder,repmat(cylinderLengthsSC,length(slewvec),1),LCNameMatrix{s},'AP2');
                            xlswritefast(filePath_per_cylinder,cylinderLengthsCET(1:find(cylinderLengthsCET(:,1),1,'last'),:),LCNameMatrix{s},'AP2');
                            
                            waitbar(3/17,waitBar,sprintf('...Writing to file... (%d/%d)',s,steps));
                            xlswritefast(filePath_per_cylinder,{'F1','F2','F3','F4','F5','F6'},LCNameMatrix{s},'Z1');
                            waitbar(4/17,waitBar,sprintf('...Writing to file... (%d/%d)',s,steps));
                            xlswritefast(filePath_per_cylinder,FMatrix(1:find(FMatrix(:,1,s), 1, 'last'),:,s),LCNameMatrix{s},'Z2');
                            waitbar(5/17,waitBar,sprintf('...Writing to file... (%d/%d)',s,steps));
                            xlswritefast(filePath_per_cylinder,{'F input','Gangway', 'Slideway';'Fx [kN]',...
                                round(InputMatrix(4,s)),round(InputMatrix(10,s));'Fy [kN]',round(InputMatrix(5,s)),...
                                round(InputMatrix(11,s));'Fz [kN]', round(InputMatrix(6,s)),round(InputMatrix(12,s));...
                                'Mx [kNm]',round(InputMatrix(7,s)),round(InputMatrix(13,s));'My [kNm]',...
                                round(InputMatrix(8,s)),round(InputMatrix(14,s));'Mz [kNm]',round(InputMatrix(9,s)),...
                                round(InputMatrix(15,s)); 'Slideway on arm:', '', round(InputMatrix(3,s))},...
                                LCNameMatrix{s},'AG1');
                            waitbar(6/17,waitBar,sprintf('...Writing to file... (%d/%d)',s,steps));
                            
                            FmaxCET = round(100*(max(FMatrix(:,cyl,s))))*0.01;
                            FminCET = round(100*(min(FMatrix(:,cyl,s))))*0.01;
                            
                            [~,indexFmaxCET] = max(FMatrix(:,cyl,s));
                            [~,indexFminCET] = min(FMatrix(:,cyl,s));
                            if strcmp(stut_type,'Rocket')
                                if cyl == 1 || cyl == 3 || cyl == 5
                                    Fmax = round(100*(max(FMatrix(:,cyl+1,s))))*0.01;
                                    Fmin = round(100*(min(FMatrix(:,cyl+1,s))))*0.01;
                                    [~,indexFmaxCETpair] = max(FMatrix(:,cyl+1,s));
                                    [~,indexFminCETpair] = min(FMatrix(:,cyl+1,s));
                                elseif cyl == 2 || cyl == 4
                                    Fmax = round(100*(max(FMatrix(:,cyl-1,s))))*0.01;
                                    Fmin = round(100*(min(FMatrix(:,cyl-1,s))))*0.01;
                                    [~,indexFmaxCETpair] = max(FMatrix(:,cyl-1,s));
                                    [~,indexFminCETpair] = min(FMatrix(:,cyl-1,s));
                                elseif cyl == 6
                                    Fmax = round(100*(max(FMatrix(:,1,s))))*0.01;
                                    Fmin = round(100*(min(FMatrix(:,1,s))))*0.01;
                                    [~,indexFmaxCETpair] = max(FMatrix(:,1,s));
                                    [~,indexFminCETpair] = min(FMatrix(:,1,s));
                                end
                            end
                            
                            waitbar(15/17,waitBar,sprintf('...Writing to file... (%d/%d)',s,steps));
                            xlswritefast(filePath_per_cylinder,{'Workspace SC',[]; 'ZMin [m]', -(hexapod.control.z_n - hexapod.geometry.minHeight);},LCNameMatrix{s},'AK14')
                            
                            xlswritefast(filePath_per_cylinder,{'Slew angles',[],[],[];[],'Min','Step size','Max';'',slewMin,slewStep,slewMax;},LCNameMatrix{s},'AK17')
                            waitbar(15/17,waitBar,sprintf('...Writing to file... (%d/%d)',s,steps));
                            
                            xlswritefast(filePath_per_cylinder,{'Basic Geometrics Parameters',[],[];'Stroke',hexapod.geometry.stroke,'m';'Orientation','?',[];'Rt',hexapod.geometry.R_t,'m';'Rb',hexapod.geometry.R_b,'m';'St',hexapod.geometry.s_t,'m';'Sb',hexapod.geometry.s_b,'m';'Dead Length',hexapod.geometry.dead_length,'m'; 'Min Height',hexapod.geometry.minHeight,'m'; 'Max Height', hexapod.geometry.maxHeight,'m'},LCNameMatrix{s},'AK21');
                            waitbar(16/17,waitBar,sprintf('...Writing to file... (%d/%d)',s,steps));
                            xlswritefast(filePath_per_cylinder,{'Exchange of cylinder',cyl,[];'F output',[],'Angle [deg]' ;'Fmin in CET [kN]',FminCET, WSCET(indexFminCET,end);'Fmax in CET [kN]',FmaxCET, WSCET(indexFmaxCET,end)},LCNameMatrix{s},'AG10');
                            if strcmp(stut_type,'Rocket')
                                xlswritefast(filePath_per_cylinder,{'Forces in corresponding cylinder pair',[],'Angle [deg]';'Fmin [kN]',Fmin,WSCET(indexFminCETpair,end);'Fmax [kN]',Fmax,WSCET(indexFmaxCETpair,end)},LCNameMatrix{s},'AG15');
                            end
                            waitbar(17/17,waitBar,sprintf('...Writing to file... (%d/%d)',s,steps));
                        end
                        
                        
                        % end the xls write procedure
                        invoke(Excel.ActiveWorkbook,'Save');
                    end % end loop over all cylinders
                    Excel.Quit
                    Excel.delete
                    clear Excel
                    
                    waitbar(9/9,waitBar,'...Almost done...');
                    close(waitBar)
                    counter_overall = counter_overall + 1;
                end % end sw_leg
            end %end sw loop
        end % end SF
    end % end acc loop
end % end GW loop
% quit all excel processes
system('taskkill /F /IM EXCEL.EXE'); % hard command the system to the quit excel
%%%

guidata(hObject, handles);
%% Remove all data (if user wants to)
delete(overallProgress);
waitfor(msgbox('Done!'));
button = questdlg('Would you like to save the parameters?','Save Parameters','Yes','No','No');
if strcmp(button,'No')
    
    rmappdata(0,'hexapod');
    rmappdata(0,'stut_type');
    rmappdata(0,'InputMatrix');
    rmappdata(0,'LCNameMatrix');
    set(handles.textNrLC,'String', 'Number set: 0');
    if isempty(getappdata(0,'extensionLength'))== false
        rmappdata(0,'extensionLength');
    end
    if isempty(getappdata(0,'cylinderselection'))== false
        rmappdata(0,'cylinderselection');
    end
end

%% Opening the file?
button = questdlg('Would you like to open the file?','Open File','Yes','No','Yes');
if strcmp(button,'Yes')
    winopen(filePath_per_cylinder);
end



%%
%%%%%%%%%%%%%%% UIPANEL BASIC GEOMETRIC PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%%
% --------------------------------------------------------------------
function uibuttongroupGeometrics_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to uibuttongroupGeometrics (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
switch get(eventdata.NewValue,'Tag') % Get Tag of selected object.
    case 'rbAtype'
        % Code for when rbAtype is selected.
        hexapod = Hexapod('A');
        setappdata(0,'hexapod',hexapod);
        if isappdata(0,'WSMCdone')
            rmappdata(0,'WSMCdone');
        end
        if isappdata(0,'WSSEdone')
            rmappdata(0,'WSSEdone');
        end
    case 'rbEtype'
        % Code for when rbEtype is selected.
        hexapod = Hexapod('E');
        setappdata(0,'hexapod',hexapod);
        if isappdata(0,'WSMCdone')
            rmappdata(0,'WSMCdone');
        end
        if isappdata(0,'WSSEdone')
            rmappdata(0,'WSSEdone');
        end
    case 'rbLtype'
        % Code for when rbLtype is selected.
        % NOT YET AVAILABLE
    case 'rbSelf'
        % Code for when rbSelf is selected.
        geometry = inputdlg({'Stroke','Orientation', 'R,top', 'R,base','St','Sb','Dead Length' },'Self defined basic geometric parameters',1);
        hexapod = Hexapod(geometry{1}, geometry{2}, geometry{3}, geometry{4}, geometry{5}, geometry{6}, geometry{7});
        hexapod.geometry.type = [];
        % pressing cancel button
        if isempty(hexapod.geometry)
            return;
        end
        %pressing ok button
        while isnan(sum(str2double(geometry)))
            uiwait(errordlg('You must enter numerical values for every parameter', 'Bad Input', 'modal'));
            geometry = (inputdlg({'Stroke','Orientation', 'R,top', 'R,base','St','Sb','Dead Length' },'Self defined basic geometric parameters',1,geometry));
            hexapod = Hexapod(geometry{1}, geometry{2}, geometry{3}, geometry{4}, geometry{5}, geometry{6}, geometry{7});
            hexapod.geometry.type = [];
            if isempty(hexapod.geometry)
                return;
            end
        end
        setappdata(0,'hexapod',hexapod);
        if isappdata(0,'WSMCdone')
            rmappdata(0,'WSMCdone');
        end
        if isappdata(0,'WSSEdone')
            rmappdata(0,'WSSEdone');
        end
end

% save the changes to the structure
guidata(hObject,handles);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isempty(getappdata(0,'Fx'))== false
    rmappdata(0,'Fx');
end
if isempty(getappdata(0,'Fy'))== false
    rmappdata(0,'Fy');
end
if isempty(getappdata(0,'Fz'))== false
    rmappdata(0,'Fz');
end
if isempty(getappdata(0,'Mx'))== false
    rmappdata(0,'Mx');
end
if isempty(getappdata(0,'My'))== false
    rmappdata(0,'My');
end
if isempty(getappdata(0,'Mz'))== false
    rmappdata(0,'Mz');
end

if isempty(getappdata(0,'xvec'))== false
    rmappdata(0,'xvec');
end
if isempty(getappdata(0,'yvec'))== false
    rmappdata(0,'yvec');
end
if isempty(getappdata(0,'zvec'))== false
    rmappdata(0,'zvec');
end
if isempty(getappdata(0,'zvecSE'))== false
    rmappdata(0,'zvecSE');
end
if isempty(getappdata(0,'phivec'))== false
    rmappdata(0,'phivec');
end
if isempty(getappdata(0,'thetavec'))== false
    rmappdata(0,'thetavec');
end
if isempty(getappdata(0,'psivec'))== false
    rmappdata(0,'psivec');
end
if isempty(getappdata(0,'hexapod'))== false
    rmappdata(0,'hexapod');
end
if isempty(getappdata(0,'XMin'))== false
    rmappdata(0,'XMin');
end
if isempty(getappdata(0,'YMin'))== false
    rmappdata(0,'YMin');
end
if isempty(getappdata(0,'ZMin'))== false
    rmappdata(0,'ZMin');
end
if isempty(getappdata(0,'ZMinSE'))== false
    rmappdata(0,'ZMinSE');
end
if isempty(getappdata(0,'PhiMin'))== false
    rmappdata(0,'PhiMin');
end
if isempty(getappdata(0,'ThetaMin'))== false
    rmappdata(0,'ThetaMin');
end
if isempty(getappdata(0,'PsiMin'))== false
    rmappdata(0,'PsiMin');
end
if isempty(getappdata(0,'XStep'))== false
    rmappdata(0,'XStep');
end
if isempty(getappdata(0,'YStep'))== false
    rmappdata(0,'YStep');
end
if isappdata(0,'ZStep')
    rmappdata(0,'ZStep');
end
if isappdata(0,'ZStepSE')
    rmappdata(0,'ZStepSE');
end
if isappdata(0,'PhiStep')
    rmappdata(0,'PhiStep');
end
if isappdata(0,'ThetaStep')
    rmappdata(0,'ThetaStep');
end
if isappdata(0,'PsiStep')
    rmappdata(0,'PsiStep');
end
if isappdata(0,'XMax')
    rmappdata(0,'XMax');
end
if isappdata(0,'YMax')
    rmappdata(0,'YMax');
end
if isappdata(0,'ZMax')
    rmappdata(0,'ZMax');
end
if isappdata(0,'ZMaxSE')
    rmappdata(0,'ZMaxSE');
end
if isappdata(0,'PhiMax')
    rmappdata(0,'PhiMax');
end
if isappdata(0,'ThetaMax')
    rmappdata(0,'ThetaMax');
end
if isappdata(0,'PsiMax')
    rmappdata(0,'PsiMax');
end
if isappdata(0,'WSMCdone')
    rmappdata(0,'WSMCdone');
end
if isappdata(0,'WSSEdone')
    rmappdata(0,'WSSEdone');
end
if isappdata(0,'LCName')
    rmappdata(0,'LCName');
end
if isappdata(0,'LCWorkspace')
    rmappdata(0,'LCWorkspace');
end
if isappdata(0,'LCCount')
    rmappdata(0,'LCCount');
end
if isappdata(0,'InputMatrix')
    rmappdata(0,'InputMatrix');
end
if isappdata(0,'LCNameMatrix')
    rmappdata(0,'LCNameMatrix');
end
if isappdata(0,'LCOM')
    rmappdata(0,'LCOM');
end

delete(hObject);


% --- Executes on button press in pbHelp.
function pbHelp_Callback(hObject, eventdata, handles)
% hObject    handle to pbHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
waitfor(open('Help.fig'));


% --- Executes on button press in pbsetWS.
function pbsetWS_Callback(hObject, eventdata, handles)
% hObject    handle to pbsetWS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% when pushed: open new screen 'set workspaces'
if isempty(getappdata(0,'hexapod'))
    errordlg('Please select the basic geometric parameters first', 'Error')
    return
end
waitfor(open('SetWorkspace.fig'));


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in pbViewLC.
function pbViewLC_Callback(hObject, eventdata, handles)
% hObject    handle to pbViewLC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
waitfor(open('ViewLC.fig'));


% --- Executes on button press in pushbutton_LoadFromExcel.
function pushbutton_LoadFromExcel_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_LoadFromExcel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileNameLoadcases, filePath_per_cylinderLoadCases] = uigetfile('*.xls; *.xlsx', 'Select load case file');

% Check if the load case path exists
if filePath_per_cylinderLoadCases == 0
    errordlg('No load case file specified','Missing Input','Modal');
    uicontrol(hObject);
    return;
end
filename = [filePath_per_cylinderLoadCases FileNameLoadcases ];
setappdata(0,'filename', filename); % load case file


% save the changes to the structure
guidata(hObject,handles);
