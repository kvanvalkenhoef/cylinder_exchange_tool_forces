function lloydsform = case11(hexapod,forces,lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix)

top_m = hexapod.geometry.T_h;

if sum(any(forces,2)) == 1 % forces has only one row
%     % EDIT Jolien Rip: maxforce is now largest value of min and max
%     [MF1,C1] = max(forces);
%     [MF2,C2] = min(forces);
%     if abs(MF1) > abs(MF2)
%         maxforce = MF1;
%         col = C1;
%     else
%         maxforce = MF2;
%         col = C2;
%     end
%     
%     [~,row] = max(abs(maxforce));
    
    % original code
    [maxforce,col] = max(forces);
    [~,row] = max(maxforce);
    
    Maxforce = forces(row,col);
    phase = 1;
    
    Mat011 = phase;
    Mat021 = col;
    Mat031 = Maxforce;
    Mat041 = lengths(row,col);
    if InputMatrix(2,phase) == 1 % SE workspace
        Mat051 = baseMatrixSE(1,col);
        Mat061 = baseMatrixSE(2,col);
        Mat071 = baseMatrixSE(3,col);
    elseif InputMatrix(2,phase) == 2 % MC workspace
        Mat051 = baseMatrixMC(1,col);
        Mat061 = baseMatrixMC(2,col);
        Mat071 = baseMatrixMC(3,col);
    else % SC workspace
        Mat051 = baseMatrixSC(1,col);
        Mat061 = baseMatrixSC(2,col);
        Mat071 = baseMatrixSC(3,col);
    end
    Mat081 = top_m(1,col);
    Mat091 = top_m(2,col);
    Mat101 = top_m(3,col);
    if InputMatrix(2,phase) == 1 % SE workspace
        Mat111 = WSSE(1,1);
        Mat121 = WSSE(1,2);
        Mat131 = WSSE(1,3);
        Mat141 = WSSE(1,4);
        Mat151 = WSSE(1,5);
        Mat161 = WSSE(1,6);
    elseif InputMatrix(2,phase) == 2 % MC workspace
        Mat111 = WSMC(1,1);
        Mat121 = WSMC(1,2);
        Mat131 = WSMC(1,3);
        Mat141 = WSMC(1,4);
        Mat151 = WSMC(1,5);
        Mat161 = WSMC(1,6);
    else % SC workspace
        Mat111 = WSSC(1,1);
        Mat121 = WSSC(1,2);
        Mat131 = WSSC(1,3);
        Mat141 = WSSC(1,4);
        Mat151 = WSSC(1,5);
        Mat161 = WSSC(1,6);
    end
    
else
    
%     % EDIT Jolien Rip: maxforce is now largest value of min and max
%     [MF1,R1] = max(forces); % max element from each column (loadcase), row=row index of max
%     [MF2,R2] = min(forces); % min element from each column (loadcase), row=row index of max
%     if abs(MF1) > abs(MF2)
%         maxforce = MF1;
%         row = R1;
%     else
%         maxforce = MF2;
%         row = R2;
%     end
%     % max force of all loadcases, col=cylinder number
%     [Maxforce,col] = max(abs(maxforce)); 
    
    % ORIGINAL CODE
    [maxforce,row] = max(forces);
    [Maxforce,col] = max(maxforce);
    %----
    
    phase = ceil(row(col)/length(WSMC));
    
    Mat011 = phase;
    Mat021 = col;
    Mat031 = Maxforce;
    Mat041 = lengths(row(col),col);
    if InputMatrix(2,phase) == 1 % SE workspace
        Mat051 = baseMatrixSE(1,col,(row(col)-((phase-1)*length(WSMC))));
        Mat061 = baseMatrixSE(2,col,(row(col)-((phase-1)*length(WSMC))));
        Mat071 = baseMatrixSE(3,col,(row(col)-((phase-1)*length(WSMC))));
    elseif InputMatrix(2,phase) == 2 % MC workspace
        Mat051 = baseMatrixMC(1,col,(row(col)-((phase-1)*length(WSMC))));
        Mat061 = baseMatrixMC(2,col,(row(col)-((phase-1)*length(WSMC))));
        Mat071 = baseMatrixMC(3,col,(row(col)-((phase-1)*length(WSMC))));
    else % SC workspace
        Mat051 = baseMatrixSC(1,col,(row(col)-((phase-1)*length(WSMC))));
        Mat061 = baseMatrixSC(2,col,(row(col)-((phase-1)*length(WSMC))));
        Mat071 = baseMatrixSC(3,col,(row(col)-((phase-1)*length(WSMC))));
    end
    Mat081 = top_m(1,col);
    Mat091 = top_m(2,col);
    Mat101 = top_m(3,col);
    if InputMatrix(2,phase) == 1 % SE workspace
        Mat111 = WSSE((row(col)-((phase-1)*length(WSMC))),1);
        Mat121 = WSSE((row(col)-((phase-1)*length(WSMC))),2);
        Mat131 = WSSE((row(col)-((phase-1)*length(WSMC))),3);
        Mat141 = WSSE((row(col)-((phase-1)*length(WSMC))),4);
        Mat151 = WSSE((row(col)-((phase-1)*length(WSMC))),5);
        Mat161 = WSSE((row(col)-((phase-1)*length(WSMC))),6);
    elseif InputMatrix(2,phase) == 2 % MC workspace
        Mat111 = WSMC((row(col)-((phase-1)*length(WSMC))),1);
        Mat121 = WSMC((row(col)-((phase-1)*length(WSMC))),2);
        Mat131 = WSMC((row(col)-((phase-1)*length(WSMC))),3);
        Mat141 = WSMC((row(col)-((phase-1)*length(WSMC))),4);
        Mat151 = WSMC((row(col)-((phase-1)*length(WSMC))),5);
        Mat161 = WSMC((row(col)-((phase-1)*length(WSMC))),6);
    else % SC workspace
        Mat111 = WSSC((row(col)-((phase-1)*length(WSMC))),1);
        Mat121 = WSSC((row(col)-((phase-1)*length(WSMC))),2);
        Mat131 = WSSC((row(col)-((phase-1)*length(WSMC))),3);
        Mat141 = WSSC((row(col)-((phase-1)*length(WSMC))),4);
        Mat151 = WSSC((row(col)-((phase-1)*length(WSMC))),5);
        Mat161 = WSSC((row(col)-((phase-1)*length(WSMC))),6);
    end
end

if Mat031 == 0 % there are no loadcases in this operational mode
    lloydsform = zeros(16,1);
    lloydsform(1,1) = size(InputMatrix,2) + 1;
else
    lloydsform(:,1) = [Mat011 Mat021 Mat031 Mat041 Mat051 Mat061 Mat071 Mat081 Mat091 Mat101 Mat111 Mat121 Mat131 Mat141 Mat151 Mat161];
end
