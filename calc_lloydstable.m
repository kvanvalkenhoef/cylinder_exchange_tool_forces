WSMC = workspaceOutputMC;
WSSC = workspaceOutputSC;
WSSE = workspaceOutputSE;

%% split forces into NO, EC and SC conditions
waitbar(1/9,waitBar,'...Determining output tables...');

ForcesNO = [];
ForcesEC = [];
ForcesSC = [];

for j = 1:size(InputMatrix,2)
    if InputMatrix(1,j) == 1
        ForcesNO = [ForcesNO; FMatrix(:,:,j)];
        ForcesEC = [ForcesEC; zeros(size(FMatrix,1),size(FMatrix,2))];
        ForcesSC = [ForcesSC; zeros(size(FMatrix,1),size(FMatrix,2))];
    elseif InputMatrix(1,j) == 2
        ForcesEC = [ForcesEC; FMatrix(:,:,j)];
        ForcesNO = [ForcesNO; zeros(size(FMatrix,1),size(FMatrix,2))];
        ForcesSC = [ForcesSC; zeros(size(FMatrix,1),size(FMatrix,2))];
    else
        ForcesEC = [ForcesEC; zeros(size(FMatrix,1),size(FMatrix,2))];
        ForcesNO = [ForcesNO; zeros(size(FMatrix,1),size(FMatrix,2))];
        ForcesSC = [ForcesSC; FMatrix(:,:,j)];
    end
end

%% create matrix with all cylinder lengths
waitbar(2/9,waitBar,'...Determining output tables...');
Lengths = [];

for j = 1:size(InputMatrix,2)
        if InputMatrix(2,j) == 1
            Lengths = [Lengths; cylinderLengthsSE];
        elseif InputMatrix(2,j) == 2
            Lengths = [Lengths; cylinderLengthsMC];
        else
            Lengths = [Lengths; cylinderLengthsSC];
        end
end

%% calculate buckling matrix
% scale forces matrix to F*l^2
waitbar(3/9,waitBar,'...Determining output tables...');

BuckMatrix = zeros(size(FMatrix,1), 6, size(FMatrix,3));

for i = 1 : size(FMatrix,3)
    k = find(FMatrix(:,1,i),1,'last');
     if InputMatrix(2,i) == 1 % forces are in SE workspace; use LSE
         BuckMatrix(1:k,:,i) = FMatrix(1:k,:,i).*cylinderLengthsSE(1:k,:).^2;
     elseif InputMatrix(2,i) == 2 % forces are in MC workspace; use LMC
         BuckMatrix(1:k,:,i) = FMatrix(1:k,:,i).*cylinderLengthsMC(1:k,:).^2;
     else % forces are in SC workspace; use LSC
         BuckMatrix(1:k,:,i) = FMatrix(1:k,:,i).*cylinderLengthsSC(1:k,:).^2;
     end
     
%     for k = 1 : find(FMatrix(:,1,i),1,'last')
%         if InputMatrix(2,i) == 1 % forces are in SE workspace; use LSE
%             for j = 1 : 6
%                 BuckMatrix(k,j,i) = FMatrix(k,j,i) * cylinderLengthsSE(k,j)^2;
%             end
%         elseif InputMatrix(2,i) == 2 % forces are in MC workspace; use LMC
%             for j = 1 : 6
%                 BuckMatrix(k,j,i) = FMatrix(k,j,i) * cylinderLengthsMC(k,j)^2;
%             end
%         else % forces are in SC workspace; use LSC
%             for j = 1 : 6
%                 BuckMatrix(k,j,i) = FMatrix(k,j,i) * cylinderLengthsSC(k,j)^2;
%             end
%         end
%     end
end

%% split buckling forces NO, EC and SC conditions
waitbar(4/9,waitBar,'...Determining output tables...');

ForcesBuckNO = [];
ForcesBuckEC = [];
ForcesBuckSC = [];

for j = 1:size(InputMatrix,2)
    if InputMatrix(1,j) == 1 % set values at NO, zeros at EC
        ForcesBuckNO = [ForcesBuckNO; BuckMatrix(:,:,j)];
        ForcesBuckEC = [ForcesBuckEC; zeros(size(BuckMatrix,1),size(BuckMatrix,2))];
        ForcesBuckSC = [ForcesBuckSC; zeros(size(BuckMatrix,1),size(BuckMatrix,2))];
    elseif InputMatrix(1,j) == 2
        ForcesBuckEC = [ForcesBuckEC; BuckMatrix(:,:,j)];
        ForcesBuckNO = [ForcesBuckNO; zeros(size(BuckMatrix,1),size(BuckMatrix,2))];
        ForcesBuckSC = [ForcesBuckSC; zeros(size(BuckMatrix,1),size(BuckMatrix,2))];
    else
        ForcesBuckSC = [ForcesBuckSC; BuckMatrix(:,:,j)];
        ForcesBuckNO = [ForcesBuckNO; zeros(size(BuckMatrix,1),size(BuckMatrix,2))];
        ForcesBuckEC = [ForcesBuckEC; zeros(size(BuckMatrix,1),size(BuckMatrix,2))];
    end
end

%% calculate delta z matrix
waitbar(5/9,waitBar,'...Determining output tables...');

ZMatrix = zeros(size(FMatrix,1), 6, size(FMatrix,3));

for i = 1 : size(FMatrix,3)
    k = find(FMatrix(:,1,i),1,'last');
    if InputMatrix(2,i) == 1 % forces are in SE workspace; use LSE
        if k==1
            ZMatrix(1,:,i) = FMatrix(1,:,i).*cylinderUnitVectorSE(3,:,1);
        else
            ZMatrix(1:k,:,i) = FMatrix(1:k,:,i).*squeeze(cylinderUnitVectorSE(3,:,1:k))';
        end
     elseif InputMatrix(2,i) == 2 % forces are in MC workspace; use LMC
         if k==1
            ZMatrix(1,:,i) = FMatrix(1,:,i).*cylinderUnitVectorMC(3,:,1);
         else
            ZMatrix(1:k,:,i) = FMatrix(1:k,:,i).*squeeze(cylinderUnitVectorMC(3,:,1:k))';
         end
     else % forces are in SC workspace; use LSC
         if k==1
            ZMatrix(1,:,i) = FMatrix(1,:,i).*cylinderUnitVectorSC(3,:,1);
         else
            ZMatrix(1:k,:,i) = FMatrix(1:k,:,i).*squeeze(cylinderUnitVectorSC(3,:,1:k))';
         end
     end
%     
%     for k = 1 : find(FMatrix(:,1,i),1,'last')
%         if InputMatrix(2,i) == 1 % forces are in SE workspace; use CSE
%             for j = 1 : 6
%                 ZMatrix(k,j,i) = FMatrix(k,j,i) * cylinderUnitVectorSE(3,j,k);
%             end
%         elseif InputMatrix(2,i) == 2 % forces are in MC workspace; use CMC
%             for j = 1 : 6
%                 ZMatrix(k,j,i) = FMatrix(k,j,i) * cylinderUnitVectorMC(3,j,k);
%             end
%         else % forces are in SC workspace; use CSC
%             for j = 1 : 6
%                 ZMatrix(k,j,i) = FMatrix(k,j,i) * cylinderUnitVectorSC(3,j,k);
%             end
%         end
%     end
end

%% split delta z in NO, EC and SC conditions
waitbar(6/9,waitBar,'...Determining output tables...');

ForcesZNO = [];
ForcesZEC = [];
ForcesZSC = [];

for j = 1:size(InputMatrix,2)
    if InputMatrix(1,j) == 1
        ForcesZNO = [ForcesZNO; ZMatrix(:,:,j)];
        ForcesZEC = [ForcesZEC; zeros(size(ZMatrix,1),size(ZMatrix,2))];
        ForcesZSC = [ForcesZSC; zeros(size(ZMatrix,1),size(ZMatrix,2))];
    elseif InputMatrix(1,j) == 2
        ForcesZEC = [ForcesZEC; ZMatrix(:,:,j)];
        ForcesZNO = [ForcesZNO; zeros(size(ZMatrix,1),size(ZMatrix,2))];
        ForcesZSC = [ForcesZSC; zeros(size(ZMatrix,1),size(ZMatrix,2))];
    else
        ForcesZSC = [ForcesZSC; ZMatrix(:,:,j)];
        ForcesZNO = [ForcesZNO; zeros(size(ZMatrix,1),size(ZMatrix,2))];
        ForcesZEC = [ForcesZEC; zeros(size(ZMatrix,1),size(ZMatrix,2))];        
    end
end

%% calculate LLoyds table for NO forces
waitbar(7/9,waitBar,'...Determining output tables...');

Lloydsform1(:,1) = case11(hexapod,ForcesNO,Lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix);
Lloydsform1(:,2) = case12(hexapod,ForcesBuckNO,ForcesNO,Lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix);
Lloydsform1(:,3:4) = zeros(16,2); 

Lloydsform2(:,1) = case21(hexapod,ForcesZNO, ForcesNO,Lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix);
Lloydsform2(:,2) = case22(hexapod,ForcesZNO, ForcesNO,Lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix);
Lloydsform2(:,3) = case23(hexapod,ForcesZNO, ForcesNO,Lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix);
Lloydsform2(:,4) = case24(hexapod,ForcesZNO, ForcesNO,Lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix);

%total table for NO forces
Lloydsform12 = [Lloydsform1; zeros(2,4); Lloydsform2];

%% calculate LLoyds table for EC forces
waitbar(8/9,waitBar,'...Determining output tables...');
Lloydsform3(:,1) = case11(hexapod,ForcesEC,Lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix);
Lloydsform3(:,2) = case12(hexapod,ForcesBuckEC,ForcesEC,Lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix);
Lloydsform3(:,3:4) = zeros(16,2); 

Lloydsform4(:,1) = case21(hexapod,ForcesZEC, ForcesEC,Lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix);
Lloydsform4(:,2) = case22(hexapod,ForcesZEC, ForcesEC,Lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix);
Lloydsform4(:,3) = case23(hexapod,ForcesZEC, ForcesEC,Lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix);
Lloydsform4(:,4) = case24(hexapod,ForcesZEC, ForcesEC,Lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix);
%total table for EC forces
Lloydsform34 = [Lloydsform3; zeros(2,4); Lloydsform4];

%% calculate LLoyds table for SC forces
waitbar(9/9,waitBar,'...Determining output tables...');
Lloydsform5(:,1) = case11(hexapod,ForcesSC,Lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix);
Lloydsform5(:,2) = case12(hexapod,ForcesBuckSC,ForcesSC,Lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix);
Lloydsform5(:,3:4) = zeros(16,2); 

Lloydsform6(:,1) = case21(hexapod,ForcesZSC, ForcesSC,Lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix);
Lloydsform6(:,2) = case22(hexapod,ForcesZSC, ForcesSC,Lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix);
Lloydsform6(:,3) = case23(hexapod,ForcesZSC, ForcesSC,Lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix);
Lloydsform6(:,4) = case24(hexapod,ForcesZSC, ForcesSC,Lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix);

%total table for SC forces
Lloydsform56 = [Lloydsform5; zeros(2,4); Lloydsform6];

%% Make total Lloyds 
waitbar(9/9,waitBar,'...Determining output tables...');

Lloydsformtotal = [Lloydsform12; zeros(3,4); Lloydsform34; zeros(3,4); Lloydsform56];
