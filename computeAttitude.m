function [workspaceOutput, cylinderLengths, cylinderUnitVector, baseMatrix,T_t1] = computeAttitude(hex,cylinder,push_out_length,stepsize,stut_type)
% MATLAB code for Hexapod_Attitude
% Description: The code determines the attitude of the hexapod when given a certain cylinder length and one cylinder is moved out or in.
%
% SYNOPSIS: [workspaceOutput, cylinderLengths, cylinderUnitVector, baseMatrix] = computeAttitude(hex,cylinder,push_out_length,stepsize)
%
% INPUT
% hexapod:                  object of class Hexapod
% cylinder                  the cylinder number to be exchanged in the loop
% push_out_length           the length the cylinder is desired to be extended during the exchange
% stepsize                  number of steps considered during the extension

% OUTPUT

% Edit the above text to modify the response to help GUI

% Last Modified by

% Begin initialization code
q0 = [0 0 0 0 0 0];
% Gimbal coordinates
T_t = getUpperGimbalCoordinates(hex);% upper gimbal coordinaties
B_f = getLowerGimbalCoordinates(hex);% lower gimbal

% Bottom and top frame definition
if hex.geometry.stroke == 2
    T_t(3,:) = T_t(3,:)-1.25; % distance between neutral and settled
elseif hex.geometry.stroke > 2
    T_t(3,:) = T_t(3,:)-1.93; % distance between neutral and settled
end
B_f(3,:) = B_f(3,:)-T_t(3,:);
T_t(3,:) = 0;

% Cylinder characteristics
l_stowed = hex.geometry.stroke + hex.geometry.dead_length; % Cylinder length in stowed condition in meters
l_in     = ones(1,6)*l_stowed; % cylinder lengths in a certain position for all cylinders in meters

Counter = 1;
% determine the required attitude with the
for ii = 1:stepsize
    % cylinder pairs at the top: 1-2, 3-4, 5-6
    % cylinder pairs at the bottom: 1-6, 2-3, 4-5
    if strcmp(stut_type,'Rocket')
        if cylinder == 1 || cylinder == 2
            l_in(1,1:2) = l_stowed+push_out_length*ii/stepsize; % move one cylinder in meters
        elseif cylinder == 3 || cylinder == 4
            l_in(1,3:4) = l_stowed+push_out_length*ii/stepsize; % move one cylinder in meters
        elseif cylinder == 5 || cylinder == 6
            l_in(1,5:6) = l_stowed+push_out_length*ii/stepsize; % move one cylinder in meters
        end
    elseif strcmp(stut_type,'Cylinder')
        l_in(1,cylinder) = l_stowed+push_out_length*ii/stepsize; % move one cylinder in meters
    end
    
    % Kinematics to determine the new hexapod attitude
    Attitude_temp = forward_kinematics(q0, B_f, l_in, T_t, 10);    % Determine the attitude of the hexapod
    Attitude = Attitude_temp(2,:);
    
    
    %% Replace a cylinder by the CET now that the attitude is known - in neutral
    % CET coordinates
    if strcmp(stut_type,'Rocket')
        T_CET = getUpperCETRocketCoordinates(hex);
        T_CET(3,:) = 0;
        % replace the to be exchanged cylinder with the coordinates of the CET - top and botom
        T_t(:,cylinder) = T_CET(:,cylinder);
        B_f(:,cylinder) = T_CET(:,cylinder);
        B_f(3,:) = 0;
    elseif strcmp(stut_type,'Cylinder')
        T_CET = getUpperCETCylinderCoordinates(hex);
        B_CET = getLowerCETCylinderCoordinates(hex);
        T_CET(3,:) = 0;
        % replace the to be exchanged cylinder with the coordinates of the CET - top and botom
        T_t(:,cylinder) = T_CET(:,cylinder);
        B_f(:,cylinder) = B_CET(:,cylinder);
        B_f(3,:) = 0;
    end

    % difference between top and bottom
    diff = T_t - B_f;    
    
    % vector to adjust top gimbal positions
    vec_a = [0*ones(1,6); 0*ones(1,6); sqrt(hex.geometry.minCylinderLength^2 - (diff(1,1)^2 + diff(2,1)^2))*ones(1,6)];
    
    %rotation matrix
    R = [cos(Attitude(6))*cos(Attitude(5))                                                      sin(Attitude(6))*cos(Attitude(5))                                                       -sin(Attitude(5))
        cos(Attitude(6))*sin(Attitude(5))*sin(Attitude(4))-sin(Attitude(6))*cos(Attitude(4))   sin(Attitude(6))*sin(Attitude(5))*sin(Attitude(4))+cos(Attitude(6))*cos(Attitude(4))    cos(Attitude(5))*sin(Attitude(4))
        cos(Attitude(6))*sin(Attitude(5))*cos(Attitude(4))+sin(Attitude(6))*sin(Attitude(4))   sin(Attitude(6))*sin(Attitude(5))*cos(Attitude(4))-cos(Attitude(6))*sin(Attitude(4))    cos(Attitude(5))*cos(Attitude(4))];
    
    % determine the top gimbal positions with changed attitude
    T_t1 = R' * T_t + [Attitude(1)*ones(1,6); Attitude(2)*ones(1,6); Attitude(3)*ones(1,6)] + vec_a;
    
    % adjust to hexapod forces method by making the bottom frame the moving frame
    fixedFrameFixed  = [T_t1(1:2,:); T_t1(3,:)-T_t1(3,:)];
    movingFrameFixed = [B_f(1:2,:); B_f(3,:)-T_t1(3,:)];
    
    % determine the cylinder vectors - difference between fixed and moving frame (reversed top and bottom)
    cylinderVector = fixedFrameFixed - movingFrameFixed;
    
    % determine the cylinder lengts
    cylinderLengthsIt = sqrt(sum(cylinderVector.^2,1));
    
    if ~isempty(cylinderLengthsIt)
        cylinderUnitVectorIt = cylinderVector./(cylinderLengthsIt'*ones(1,3))';
        workspaceOutput(Counter,:) = Attitude; % workspace, rotations in degrees
        cylinderLengths(Counter,:) = cylinderLengthsIt; % cylinder lenghts corresponding to workspace
        cylinderUnitVector(:,:,Counter) = cylinderUnitVectorIt; % directional coordinates of cylinders
        baseMatrix(:,:,Counter) = movingFrameFixed; % coordinates of base frame
        Counter = Counter + 1;
    end
end
