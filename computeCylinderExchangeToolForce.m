function [cylinderForces, cylinderUnitVector] = computeCylinderExchangeToolForce( hexapod, type, input, load_case, slew, SWArm, SWloadcase, T_f, Attitude)
% COMPUTECYLINDERFORCES calculates the six cylinder forces and their direction for a series of (bottom) frame
% positions/unit vectors and load cases. When multiple positions are
% given as input, all combinations are calculated.
%
% SYNOPSIS: [cylinderForces, cylinderUnitVector] = cylforce( Hexapod, type, input, loadcase, slew, SWarm, SWloadcase, cyl )
%
% INPUT
% hexapod:                  object of class Hexapod
% type:                     String indicating if input is 'position' or
%                           'unitvector'
% input - unitvector:       3x6xn unit vector matrix
% input - position:         nx6 matrix containing 6 DoF of the bottom frame
%                           [x (m), y (m), z (m), phi (deg), theta (deg),  psi (deg)]
% loadcase:                 6x1 matrix containing a loadcase. [Fx; Fy; Fz; Mx; My; Mz] in
%                           kN or kNm
% slew:                     slew angle (deg)
% SWArm:                    Position of the slideway on the hexapod [-]
% SWloadcase                6x1 matrix containing a loadcase present on the star frame,
%                           as a result of the connection with the slideway. [Fx; Fy; Fz; Mx; My; Mz] in
%                           kN or kNm
% cylinder                  the number of cylinder that is calculated in the loop to be exchanged
% 
%
% OUTPUT
% cylinderForces:           nx6 matrix containing the cylinder forces [kN]
%                           resulting from the input parameters [cyl1 cyl2 cyl cyl4 cyl5 cyl6]
% cylinderUnitVector:       3x6xn matrix presenting the direction of the
%                           cylinders, resulting from the input parameters.
%                           cols: [cyl1 cyl2 cyl3 cyl4 cyl5 cyl6]
%                           rows: [x; y; z;]
%
% REMARKS
% Please do not redistribute this code without permission from the author.
%
% Created with MATLAB ver.: 8.1.0.604 (R2013a)
% Copyright (c) 2014 Ampelmann Operations B.V.
% All rights reserved.
%
% CHANGELOG
% Date 		 Author 		           Comment
% 20150406   K. van Valkenhoef         Copy of computeCylinderForces
% 20150406   K. van Valkenhoef         Adjustment to CET forces
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% detect input type
if strcmp(type,'position')
    position = input;
elseif strcmp(type, 'unitvector')
    cylinderUnitVector = input;
else
    error('Please enter type as "position" or "unitvector"')
end

% convert slew degrees to radians
if length(slew) > 1
    error('Please enter 1 slew angle at the time')
else
    slew = slew * pi/180;
end

%rotation matrix
R = [cos(Attitude(6))*cos(Attitude(5))                                                      sin(Attitude(6))*cos(Attitude(5))                                                       -sin(Attitude(5))
     cos(Attitude(6))*sin(Attitude(5))*sin(Attitude(4))-sin(Attitude(6))*cos(Attitude(4))   sin(Attitude(6))*sin(Attitude(5))*sin(Attitude(4))+cos(Attitude(6))*cos(Attitude(4))    cos(Attitude(5))*sin(Attitude(4))
     cos(Attitude(6))*sin(Attitude(5))*cos(Attitude(4))+sin(Attitude(6))*sin(Attitude(4))   sin(Attitude(6))*sin(Attitude(5))*cos(Attitude(4))-cos(Attitude(6))*sin(Attitude(4))    cos(Attitude(5))*cos(Attitude(4))];
% Rotate the load case 
loadcase(1:3,1) = R * load_case(1:3);
loadcase(4:6,1) = R * load_case(4:6);
% determine position of slideway on hexapod
% determine angle w.r.t. slewing ring
if SWArm == 1;
    beta = 60 * pi/180;
else if SWArm == 2;
        beta = 180 * pi/180;
    else if SWArm ==3;
            beta = 300 * pi/180;
        else beta = 0;
        end
    end
end

%determine slideway lever arm coordinates (=upper gimbal level & radius)
SWx = hexapod.geometry.R_t * cos(beta); %2.75 * cos(beta);
SWy = hexapod.geometry.R_t * sin(beta); %2.75 * sin(beta);
SWz = 0;

% compute slew rotation matrix
slewRotMat = [ cos(-slew) -sin(-slew) 0;...
                sin(-slew) cos(-slew) 0; 0 0 1];

% compute loadcase including slew force
% Gangway slewed load case
Fgangslew(1:3,:) = slewRotMat'*loadcase(1:3);
Fgangslew(4:6,:) = slewRotMat'*loadcase(4:6);

% Slideway slewed load case
FSWslew(1:3,:) = slewRotMat'*SWloadcase(1:3);
FSWslew(4:6,:) = slewRotMat'*SWloadcase(4:6);

% compute forces centre slewing ring - slideway component
FSWcentre(1:3,:) = FSWslew(1:3);
FSWcentre(4) = FSWslew(4) + FSWslew(3)*SWy - FSWslew(2)*SWz;
FSWcentre(5) = FSWslew(5) - FSWslew(3)*SWx + FSWslew(1)*SWz;
FSWcentre(6) = FSWslew(6) - FSWslew(1)*SWy + FSWslew(2)*SWx;

%Samenvoegen gangway + slideway
Fslew(1:6,1) = Fgangslew + FSWcentre;


% calculate forces if input is of type position
if strcmp(type,'position')
    % Initialise output vectors
    cylinderUnitVector = zeros(3,6,size(position,1));
    Counter = 0;
    
    for i = 1:size(position,1)
        
        % compute cylinder length and length vector
        [ cylinderLengths, ~, cylinderVector] = inverseKinematics( hexapod, position(i, :) );
        
        if ~isempty(cylinderLengths)
            Counter = Counter + 1;
            
            % compute directional unit vector of the cylinders and matrix
            cylinderUnitVector(:,:,Counter) = cylinderVector./(cylinderLengths'*ones(1,3))';
            
            M = [ cylinderUnitVector(1,:,Counter);
                cylinderUnitVector(2,:,Counter);
                cylinderUnitVector(3,:,Counter);
                cylinderUnitVector(3,:,Counter).*T_f(2,:);
                -cylinderUnitVector(3,:,Counter).*T_f(1,:);
                cylinderUnitVector(2,:,Counter).*T_f(1,:)- cylinderUnitVector(1,:,Counter).*T_f(2,:) ];
            
            % compute cylinder force
            cylinderForces(Counter,:) = (-eye(size(M))/M*Fslew)';
            
        end
    end
    
% calculate forces if input is of type unitvector 
elseif strcmp(type,'unitvector')
    for i = 1:size(cylinderUnitVector,3)
        
        M = [ cylinderUnitVector(1,:,i);
            cylinderUnitVector(2,:,i);
            cylinderUnitVector(3,:,i);
            cylinderUnitVector(3,:,i).*T_f(2,:);
            -cylinderUnitVector(3,:,i).*T_f(1,:);
            cylinderUnitVector(2,:,i).*T_f(1,:)- cylinderUnitVector(1,:,i).*T_f(2,:) ];
        
        % compute cylinder force
        cylinderForces(i,:) = (-eye(size(M))/M*Fslew)';
    end
end
end