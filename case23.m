function lloydsform = case23(hexapod, zforces, forces,lengths,WSMC,WSSE,WSSC,baseMatrixSE,baseMatrixMC,baseMatrixSC,InputMatrix)

top_m = hexapod.geometry.T_h;

SumZetsTop = [(zforces(:,1)+zforces(:,2)) (zforces(:,3)+zforces(:,4)) (zforces(:,5)+zforces(:,6))];

if sum(any(forces,2)) == 1 % only SC, forces has only one row
%     % EDIT Jolien Rip: maxforce is now largest value of min and max
%     [MF1,C1] = max(SumZetsTop);
%     [MF2,C2] = min(SumZetsTop);
%     if abs(MF1) > abs(MF2)
%         maxforce = MF1;
%         col = C1;
%     else
%         maxforce = MF2;
%         col = C2;
%     end
%     [~,row] = max(abs(maxforce));
    
    % ORIGINAL CODE
    [maxforce,col] = max(SumZetsTop);
    [~,row] = max(maxforce);
    %------
    
    col1 = col*2-1;
    col2 = col*2;
    Maxforce1 = forces(row,col1);
    Maxforce2 = forces(row,col2);
    phase = 1;
    
    Mat011 = phase;
    Mat021 = col1;
    Mat031 = col2;
    Mat041 = Maxforce1;
    Mat051 = Maxforce2;
    Mat061 = SumZetsTop(row,col);
    Mat071 = lengths(row,col1);
    Mat081 = lengths(row,col2);
    if InputMatrix(2,phase) == 1 % SE workspace
        Mat091 = baseMatrixSE(1,col1);
        Mat101 = baseMatrixSE(2,col1);
        Mat111 = baseMatrixSE(3,col1);
    elseif InputMatrix(2,phase) == 2 % MC workspace
        Mat091 = baseMatrixMC(1,col1);
        Mat101 = baseMatrixMC(2,col1);
        Mat111 = baseMatrixMC(3,col1);
    else % SC workspace
        Mat091 = baseMatrixSC(1,col1);
        Mat101 = baseMatrixSC(2,col1);
        Mat111 = baseMatrixSC(3,col1);
    end
    Mat121 = top_m(1,col1);
    Mat131 = top_m(2,col1);
    Mat141 = top_m(3,col1);
    if InputMatrix(2,phase) == 1 % SE workspace
        Mat151 = baseMatrixSE(1,col2);
        Mat161 = baseMatrixSE(2,col2);
        Mat171 = baseMatrixSE(3,col2);
    elseif InputMatrix(2,phase) == 2 % MC workspace
        Mat151 = baseMatrixMC(1,col2);
        Mat161 = baseMatrixMC(2,col2);
        Mat171 = baseMatrixMC(3,col2);
    else % SC workspace
        Mat151 = baseMatrixSC(1,col2);
        Mat161 = baseMatrixSC(2,col2);
        Mat171 = baseMatrixSC(3,col2);
    end
    Mat181 = top_m(1,col2);
    Mat191 = top_m(2,col2);
    Mat201 = top_m(3,col2);
   if InputMatrix(2,phase) == 1 % SE workspace
        Mat211 = WSSE(1,1);
        Mat221 = WSSE(1,2);
        Mat231 = WSSE(1,3);
        Mat241 = WSSE(1,4);
        Mat251 = WSSE(1,5);
        Mat261 = WSSE(1,6);
    elseif InputMatrix(2,phase) == 2 % MC workspace
        Mat211 = WSMC(1,1);
        Mat221 = WSMC(1,2);
        Mat231 = WSMC(1,3);
        Mat241 = WSMC(1,4);
        Mat251 = WSMC(1,5);
        Mat261 = WSMC(1,6);
    else % SC workspace
        Mat211 = WSSC(1,1);
        Mat221 = WSSC(1,2);
        Mat231 = WSSC(1,3);
        Mat241 = WSSC(1,4);
        Mat251 = WSSC(1,5);
        Mat261 = WSSC(1,6);
   end
    
else
%     % EDIT Jolien Rip: maxforce is now largest value of min and max
%     [MF1,R1] = max(SumZetsTop);
%     [MF2,R2] = min(SumZetsTop);
%     if abs(MF1) > abs(MF2)
%         maxforce = MF1;
%         row = R1;
%     else
%         maxforce = MF2;
%         row = R2;
%     end
%     [~,col] = max(abs(maxforce)); 
    
    % ORIGINAL CODE
    [maxforce,row] = max(SumZetsTop);
    [~,col] = max(maxforce);
    %------
    
    col1 = col*2-1;
    col2 = col*2;
    Maxforce1 = forces(row(col),col1);
    Maxforce2 = forces(row(col),col2);
    phase = ceil(row(col)/length(WSMC));
    
    Mat011 = phase;
    Mat021 = col1;
    Mat031 = col2;
    Mat041 = Maxforce1;
    Mat051 = Maxforce2;
    Mat061 = SumZetsTop(row(col),col);
    Mat071 = lengths(row(col),col1);
    Mat081 = lengths(row(col),col2);
    
    if InputMatrix(2,phase) == 1 % SE workspace
        Mat091 = baseMatrixSE(1,col1,(row(col)-((phase-1)*length(WSMC))));
        Mat101 = baseMatrixSE(2,col1,(row(col)-((phase-1)*length(WSMC))));
        Mat111 = baseMatrixSE(3,col1,(row(col)-((phase-1)*length(WSMC))));
    elseif InputMatrix(2,phase) == 2 % MC workspace
        Mat091 = baseMatrixMC(1,col1,(row(col)-((phase-1)*length(WSMC))));
        Mat101 = baseMatrixMC(2,col1,(row(col)-((phase-1)*length(WSMC))));
        Mat111 = baseMatrixMC(3,col1,(row(col)-((phase-1)*length(WSMC))));
    else % SC workspace
        Mat091 = baseMatrixSC(1,col1,(row(col)-((phase-1)*length(WSMC))));
        Mat101 = baseMatrixSC(2,col1,(row(col)-((phase-1)*length(WSMC))));
        Mat111 = baseMatrixSC(3,col1,(row(col)-((phase-1)*length(WSMC))));
    end
    
    Mat121 = top_m(1,col1);
    Mat131 = top_m(2,col1);
    Mat141 = top_m(3,col1);
   
    if InputMatrix(2,phase) == 1 % SE workspace
        Mat151 = baseMatrixSE(1,col2,(row(col)-((phase-1)*length(WSMC))));
        Mat161 = baseMatrixSE(2,col2,(row(col)-((phase-1)*length(WSMC))));
        Mat171 = baseMatrixSE(3,col2,(row(col)-((phase-1)*length(WSMC))));
    elseif InputMatrix(2,phase) == 2 % MC workspace
        Mat151 = baseMatrixMC(1,col2,(row(col)-((phase-1)*length(WSMC))));
        Mat161 = baseMatrixMC(2,col2,(row(col)-((phase-1)*length(WSMC))));
        Mat171 = baseMatrixMC(3,col2,(row(col)-((phase-1)*length(WSMC))));
    else % SC workspace
        Mat151 = baseMatrixSC(1,col2,(row(col)-((phase-1)*length(WSMC))));
        Mat161 = baseMatrixSC(2,col2,(row(col)-((phase-1)*length(WSMC))));
        Mat171 = baseMatrixSC(3,col2,(row(col)-((phase-1)*length(WSMC))));
    end
    
    Mat181 = top_m(1,col2);
    Mat191 = top_m(2,col2);
    Mat201 = top_m(3,col2);
    
    if InputMatrix(2,phase) == 1 % SE workspace
        Mat211 = WSSE((row(col)-((phase-1)*length(WSMC))),1);
        Mat221 = WSSE((row(col)-((phase-1)*length(WSMC))),2);
        Mat231 = WSSE((row(col)-((phase-1)*length(WSMC))),3);
        Mat241 = WSSE((row(col)-((phase-1)*length(WSMC))),4);
        Mat251 = WSSE((row(col)-((phase-1)*length(WSMC))),5);
        Mat261 = WSSE((row(col)-((phase-1)*length(WSMC))),6);
    elseif InputMatrix(2,phase) == 2 % MC workspace
        Mat211 = WSMC((row(col)-((phase-1)*length(WSMC))),1);
        Mat221 = WSMC((row(col)-((phase-1)*length(WSMC))),2);
        Mat231 = WSMC((row(col)-((phase-1)*length(WSMC))),3);
        Mat241 = WSMC((row(col)-((phase-1)*length(WSMC))),4);
        Mat251 = WSMC((row(col)-((phase-1)*length(WSMC))),5);
        Mat261 = WSMC((row(col)-((phase-1)*length(WSMC))),6);
    else % SC workspace
        Mat211 = WSSC((row(col)-((phase-1)*length(WSMC))),1);
        Mat221 = WSSC((row(col)-((phase-1)*length(WSMC))),2);
        Mat231 = WSSC((row(col)-((phase-1)*length(WSMC))),3);
        Mat241 = WSSC((row(col)-((phase-1)*length(WSMC))),4);
        Mat251 = WSSC((row(col)-((phase-1)*length(WSMC))),5);
        Mat261 = WSSC((row(col)-((phase-1)*length(WSMC))),6);
    end
end

if Mat041 == 0 && Mat051 == 0 % there are no loadcases in this operational mode
    lloydsform = zeros(26,1);
    lloydsform(1,1) = size(InputMatrix,2) + 1;
else
    lloydsform(:,1) = [Mat011 Mat021 Mat031 Mat041 Mat051 Mat061 Mat071 Mat081 Mat091 Mat101 Mat111 Mat121 Mat131 Mat141 Mat151 Mat161 Mat171 Mat181 Mat191 Mat201 Mat211 Mat221 Mat231 Mat241 Mat251 Mat261];
end