function varargout = ViewLC(varargin)
% VIEWLC MATLAB code for ViewLC.fig
%      VIEWLC, by itself, creates a new VIEWLC or raises the existing
%      singleton*.
%
%      H = VIEWLC returns the handle to a new VIEWLC or the handle to
%      the existing singleton*.
%
%      VIEWLC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in VIEWLC.M with the given input arguments.
%
%      VIEWLC('Property','Value',...) creates a new VIEWLC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ViewLC_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ViewLC_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ViewLC

% Last Modified by GUIDE v2.5 08-Oct-2012 17:03:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ViewLC_OpeningFcn, ...
                   'gui_OutputFcn',  @ViewLC_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ViewLC is made visible.
function ViewLC_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ViewLC (see VARARGIN)

% Choose default command line output for ViewLC
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ViewLC wait for user response (see UIRESUME)
% uiwait(handles.figViewLC);


% --- Outputs from this function are returned to the command line.
function varargout = ViewLC_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function LCtable_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LCtable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

matrix = getappdata(0, 'InputMatrix');
set(hObject,'Data', matrix);
set(hObject,'RowName',{'OM','Workspace','Star frame arm','Fx','Fy','Fz','Mx'...
    ,'My','Mz','SWFx','SWFy','SWFz','SWMx','SWMy','SWMz'}) 
set(hObject,'ColumnName',getappdata(0,'LCNameMatrix'))
set(hObject,'ColumnEditable', true)

% save to structure
setappdata(0,'InputMatrix',get(hObject,'Data'));
guidata(hObject, handles);


% --- Executes when user attempts to close figViewLC.
function figViewLC_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figViewLC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes when entered data in editable cell(s) in LCtable.
function LCtable_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to LCtable (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)

NrColumns = size(getappdata(0,'Inputmatrix'),2);

if isnan(eventdata.NewData)
    errordlg('You can only enter numeric values!','Bad Input','Modal')
end

% save to structure
setappdata(0,'InputMatrix',get(hObject,'Data'));
guidata(hObject, handles);
